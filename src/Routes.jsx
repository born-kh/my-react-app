import React from "react";

import { Switch, Route, Redirect } from "react-router-dom";

import Profile from "./views/Profile";

import Messages from "./views/Messages";


const Routes = () => {
  return (
    <Switch>
      <Redirect exact from="/" to="/profile" />
      <Route component={Profile} path="/profile" />
      <Route component={Messages} path="/dialogs" />
    </Switch>
  );
};

export default Routes;
