import React from "react";
import clsx from "clsx";
import { withStyles } from "@material-ui/core/styles";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import MenuIcon from "@material-ui/icons/Menu";
import { IconButton } from "@material-ui/core";
import styles from "./styles"


class Header extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <AppBar
        position="absolute"
        className={clsx(classes.appBar, this.props.open && classes.appBarShift)}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="Open drawer"
            onClick={() => this.props.changeOpenDrawer(true)}
            className={clsx(
              classes.menuButton,
              this.props.open && classes.menuButtonHidden
            )}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            component="h1"
            variant="h6"
            color="inherit"
            noWrap
            className={classes.title}
          >
           {this.props.title}
          </Typography>
        </Toolbar>
      </AppBar>
    );
  }
}

export default withStyles(styles)(Header);
