import React from "react";
import { withStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Header from "../Header";
import NavBar from "../Navbar";
import { Container } from "@material-ui/core";
import styles from "./styles";

class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
    this.changeOpenDrawer = this.changeOpenDrawer.bind(this);
  }

  changeOpenDrawer(val) {
    this.setState({ open: val });
  }

  render() {

    
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <CssBaseline />
        <NavBar
          open={this.state.open}
          changeOpenDrawer={this.changeOpenDrawer}
        />
        <Header
          open={this.state.open}
          changeOpenDrawer={this.changeOpenDrawer}
          title={this.props.title}
        />

        <div className={classes.appBarSpacer} />
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Container maxWidth="lg" className={classes.container}>
            {this.props.children}
          </Container>
        </main>
      </div>
    );
  }
}

export default withStyles(styles)(Content);
