import React from "react";

import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import clsx from "clsx";
import {
  withStyles,
  IconButton,
  Drawer,
  List,
  Divider
} from "@material-ui/core";
import { mainListItems } from "../ListItems/ListItems";
import styles from "./styles";

class NavBar extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(
            classes.drawerPaper,
            !this.props.open && classes.drawerPaperClose
          )
        }}
        open={this.props.open}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={() => this.props.changeOpenDrawer(false)}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>{mainListItems}</List>
      </Drawer>
    );
  }
}

export default withStyles(styles)(NavBar);
