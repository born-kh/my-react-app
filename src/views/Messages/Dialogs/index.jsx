import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { NavLink, Link } from "react-router-dom";
import {
  Grid,
  List,
  ListItem,
  ListItemText,
  Typography,
  Paper
} from "@material-ui/core";
import { Title } from "../../../components";
import { DialogItem, Message } from "./Components";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2)
  }
}));

const Dialogs = props => {
  const classes = useStyles();
  let dialogData = [
    { name: "Sveta", id: 1 },
    { name: "Sasha", id: 2 },
    { name: "Dmitriy", id: 3 },
    { name: "Masha", id: 4 },
    { name: "Viktor", id: 5 }
  ];

  let messageData = [
    { name: "Dmitriy", message: "Hi! how are you?" },
    { name: "Me", message: "I am normal. " },
    { name: "Me", message: "And you?" },
    { name: "Dmitriy", message: "I'm fine!" }
  ];

  return (
    <div className={classes.root}>
      <Title>DIALOGS</Title>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <DialogItem dialogData={dialogData} />
        </Grid>
        <Grid item xs={4}>
          <Message messageData={messageData} />
        </Grid>
      </Grid>
    </div>
  );
};

export default Dialogs;
