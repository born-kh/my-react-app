import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { NavLink, Link } from "react-router-dom";
import {
  Grid,
  List,
  ListItem,
  ListItemText,
  Typography,
  Paper
} from "@material-ui/core";

const DialogItem = props => {
  return (
    <Paper>
      <List>
        {props.dialogData.map(data => (
          <ListItem button component={Link} to={"/dialogs" + data.id}>
            <ListItemText>{data.name}</ListItemText>
          </ListItem>
        ))}
      </List>
    </Paper>
  );
};

export default DialogItem;
