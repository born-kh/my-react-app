import React from "react";
import { Switch, Route } from "react-router-dom";
import Dialogs from "./Dialogs";
import { Content } from "../../components";

const Messages = () => {
  return (
    <Switch>
      <Content title="Messages">
        <Route exact path="/dialogs" component={Dialogs} />
      </Content>
    </Switch>
  );
};

export default Messages;
