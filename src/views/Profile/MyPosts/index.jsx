import React from "react";
import {
  List,
  ListSubheader,
  Grid,
  Button,
  Paper,
  Avatar,
  TextField
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import Post from "./Components/Posts/Post";
import styles from "./styles";
import { Title, Content } from "../../../components";


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    maxWidth: 400,
    margin: `${theme.spacing(1)}px auto`,
    padding: theme.spacing(2)
  },
  button: {
    margin: theme.spacing(1)
  }
}));

const MyPosts = props => {
  const classes = useStyles();

  let postData = [
    { message: "Hi, How are you?", likeCount: 12 },
    { message: "It's my first post", likeCount: 3 }
  ];
  return (
    <div className={classes.root}>
      <Title>My Posts</Title>

      <Grid container item xs={4}>
        <TextareaAutosize
          aria-label="minimum height"
          rows={3}
          cols={30}
          placeholder=""
        />
        <Button variant="contained" color="primary" className={classes.button}>
          Add post
        </Button>
      </Grid>

      <Grid item xs={4}>
        <Post postData={postData} />
      </Grid>
    </div>
  );
};

export default MyPosts;
