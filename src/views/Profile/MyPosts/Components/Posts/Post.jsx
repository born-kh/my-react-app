import React from "react";
import {
  List,
  ListSubheader,
  Grid,
  Paper,
  Avatar,
  TextField,
  Typography
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import userPhoto from "../../../../../assets/images/avatar.png";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    overflow: "hidden",
    padding: theme.spacing(0, 3)
  },
  paper: {
    maxWidth: 500,
    margin: `${theme.spacing(1)}px auto`,
    padding: theme.spacing(2)
  }
}));

const Post = props => {
  const classes = useStyles();
  return (
    <Paper className={classes.paper}>
      {props.postData.map(data => (
        <Grid container wrap="nowrap" spacing={3}>
          <Grid item>
            <Avatar src={userPhoto} />
            <Typography noWrap> Like {data.likeCount}</Typography>
          </Grid>
          <Grid item xs>
            <Typography noWrap>{data.message}</Typography>
          </Grid>
        </Grid>
      ))}
    </Paper>
  );
};
export default Post;
