import React from "react";
import MyPosts from "./MyPosts";
import { Switch, Route } from "react-router-dom";
import { Content } from "../../components";

const Profile = () => {
  return (
    <Switch>
      <Content title="Profile">
        <Route exact path="/profile" component={MyPosts} />
      </Content>
    </Switch>
  );
};

export default Profile;
